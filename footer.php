			<?php
				wp_reset_query();

				if (is_home() || is_front_page()) {
					$page = 'Homepage';
					$cat = '';
				} else if (is_single()) {
					$category = get_the_category();
					$cat = $category[0]->cat_name;
					$catparent = get_cat_name($cat[0]->parent);
					if(empty($catparent)) {
						$catparent = $cat;
					}
					$page = get_the_title();
				} else if (is_category()) {
					$category = get_the_category();
					$cat = $category[0]->cat_name;
					$catparent = get_cat_name($cat[0]->parent);
					if(empty($catparent)) {
						$catparent = $cat;
					}
					$page = $category[0]->cat_name . ' Category';
				} else if (is_page()) {
					$page = get_the_title();
					$cat = '';
				}
			?>
			<!-- Begin Mailchimp Signup Form -->
			<?php if ( is_single() ) : ?>
			<div id="mc_embed_signup"<?php if( has_post_thumbnail()): ?> style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>')"<?php endif; ?>>
			<?php else : ?>
			<div id="mc_embed_signup" style="background-image: url('<?php echo get_template_directory_uri(); ?>/inc/images/bloggers-desk-with-typewriter.jpg')">
			<?php endif; ?>
			<form action="https://morganmspencer.us20.list-manage.com/subscribe/post?u=afd4295f3fea9abd1970e0941&amp;id=37bdbfc248&SIGNUP=morganmspencer.com&SIGNUPPAGE=<?php echo $page; ?>&SIGNUPCATP=<?php echo $catparent; ?>&SIGNUPCAT=<?php echo $cat; ?>&CAMPAIGN=General Signup" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
				<div id="mc_embed_signup_scroll">
				<label for="mce-EMAIL">Subscribe for more <?php if(!empty($cat)) : echo $cat . ' '; endif; ?>content!</label>
				<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
				<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
				<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_afd4295f3fea9abd1970e0941_37bdbfc248" tabindex="-1" value=""></div>
				<div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
				</div>
			</form>
			</div>
			</div>
			<!--End mc_embed_signup-->

			<footer class="footer clear" role="contentinfo">
				<div class="container">
					<section class="site-info">
						<h4><a href="<?php echo site_url(); ?>"><?php bloginfo('name'); ?></a></h4>
						<p class="site-description"><?php bloginfo('description'); ?></p>
						<nav role="navigation" class="clear">

							<ul class="category-list">
								<?php

								$cat_total = count(get_categories());

								$cat_args = array(
									'orderby'	=> 'count',
									'order' 	=> 'DESC',
									'parent' 	=> 0,
								);

								$categories = get_categories($cat_args);

								foreach($categories as $category) { 
									$category_link = get_category_link( $category->term_id );?>
								<li>
									<a class="category-title" href="<?php echo esc_url($category_link); ?>" title="View all posts in <?php echo $category->name ?>"><?php echo $category->name; ?></a>

								</li>
								<?php 

								} 

								wp_reset_postdata();

								?>
								
							</ul>

						</nav>
						<p class="social">
							<a href="https://twitter.com/morganmspencer/" target="_blank"><i class="fab fa-fw fa-twitter"></i></a>
							<a href="https://www.facebook.com/morganmatthewspencer/" target="_blank"><i class="fab fa-facebook-f fa-fw"></i></a>
							<a href="https://www.linkedin.com/in/morganmspencer/" target="_blank"><i class="fab fa-fw fa-linkedin-in"></i></a>
							<a href="https://www.pinterest.com/morganmspencer/" target="_blank"><i class="fab fa-fw fa-pinterest"></i></a>
							<a href="https://m.me/morganmatthewspencer" target="_blank"><i class="fab fa-facebook-messenger fa-fw"></i></a>
							<a href="https://www.youtube.com/channel/UCR-dKb3DaCUj7ENd7HPNOeg" target="_blank"><i class="fab fa-youtube fa-fw"></i></a>
						</p>
					</section>
					
					<p class="copyright">
						&copy; <?php echo date('Y'); ?> <a href="<?php echo site_url(); ?>"><?php bloginfo('name'); ?></a> &amp; <a href="https://creativecache.co/">Creative Cache</a>. All rights reserved.
					</p>

					<?php  if ( has_nav_menu( 'legal-menu' ) ) : ?>
						<?php wp_nav_menu( array( 'theme_location' => 'legal-menu' ) ); ?>
					<?php endif; ?>
				</div>
			</footer>

		</div>

		<?php wp_footer(); ?>
	</body>
</html>
