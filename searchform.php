<form class="search" method="get" action="<?php echo home_url(); ?>" role="search">
	<input class="search-input" type="search" name="s" aria-label="Search" placeholder="Search...">
	<button class="search-submit" type="submit" role="button" name="Search">Search</button>
</form>
