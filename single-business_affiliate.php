<?php get_header(); ?>

<main role="main">
	
	<section>

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			
			<h1 class="container"><?php the_title(); ?></h1>
			
			<?php if ( has_post_thumbnail()) : ?>
				<div class="post-banner container">
				<?php the_post_thumbnail(); ?>
				</div>
			<?php endif; ?>
			
			<section class="container">
				
				<div class="entry-content">
					
					<?php the_content(); ?>
					
					<?php
					
						$affiliate_posts = wp_get_recent_posts( array( 'numberposts' => '-1', 'post_status' => 'publish', 'post_type' => 'business_affiliate', 'category__in' => wp_get_post_categories($post->ID), 'post__not_in' => array($post->ID) ) );
					
						if( $affiliate_posts ) {
					
					?>
					<section id="related-posts">
						<h3>Other Affiliate<?php if (count($affiliate_posts) > 1): ?>s<?php endif; ?></h3>
						<div>
							<?php
								foreach( $affiliate_posts as $post ) {
								setup_postdata($post);
							?>
							<div class="home-category related-post">
								<a href="<?php the_permalink(); ?>" class="post-thumbnail" title="<?php the_title(); ?>"><?php if ( has_post_thumbnail()) : ?><?php the_post_thumbnail(); ?><?php endif; ?></a>
								<span class="date"><?php the_time('F j, Y'); ?></span>
								<h3 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
							</div>
						<?php
							}
					?>
						</div>
					</section>
					<?php
						}
						wp_reset_postdata();
					?>
					
				</div>
				
				<?php get_sidebar(); ?>
				
			</section>
				

		</article>

	<?php endwhile; ?>

	<?php else: ?>

		<article>

			<h1>Sorry, nothing to display. Content coming soon!</h1>

		</article>

	<?php endif; ?>
		
	<div class="clear"></div>

	</section>
	
</main>

<?php get_footer(); ?>