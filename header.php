<!doctype html>
<?php global $post ?>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-134713414-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-134713414-1');
		</script>
		
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); if(!is_home()) :?> | <?php endif; bloginfo('name'); if(is_home()) :?> | <?php bloginfo('description'); endif; ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="author" content="Morgan Spencer">
		<meta name="google-site-verification" content="Ng79_ndHMlWka5xkIz5YOyzyFnb2GVXWaaUhb7AgBAs" />
		<meta name="p:domain_verify" content="c9f65ff0fe08e330787a59f33883f3f8"/>
		
		<?php wp_head(); ?>
		
		<link href="https://fonts.googleapis.com/css?family=Frank+Ruhl+Libre:400,500|Open+Sans:400,700" rel="stylesheet" lazyload>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous" lazyload>
		
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
			google_ad_client: "ca-pub-9620854696732686",
			enable_page_level_ads: true
		  });
		</script>
		
	</head>
	<body <?php body_class(); ?>>
		
		<header>
			
			<div class="container">
				
				<?php if (is_home()): ?>

				<h1 class="site-info">
					<a href="<?php echo site_url(); ?>"><?php bloginfo('name'); ?></a>
				</h1>

				<?php else: ?>

				<div class="site-info">
					<a href="<?php echo site_url(); ?>"><?php bloginfo('name'); ?></a>
				</div>

				<?php endif; ?>
				
				<a href="#menu" class="mobile-menu"><i class="fas fa-bars"></i> Menu</a>
				
				<nav id="main-nav" role="navigation" class="clear">
					
					<?php get_template_part('searchform'); ?>
					
					<button class="search-toggle" title="Search"><i class="fas fa-search"></i></button>
					
					<?php  if ( has_nav_menu( 'main-menu' ) ) {
						wp_nav_menu( array( 'theme_location' => 'main-menu' ) );
					} ?>
					
					
				</nav>
			
			</div>
			
			<nav id="category-nav" role="navigation" class="clear">
				
				<ul class="category-list container">
					<?php
					
					$cat_args = array(
						'orderby'	=> 'name',
						'order' 	=> 'ASC',
						'parent' 	=> 0,
					);

					$categories = get_categories($cat_args);

					foreach($categories as $category) { 
						$category_link = get_category_link( $category->term_id );?>
						<li>
						 	<a class="category-title" href="<?php echo esc_url($category_link); ?>" title="View all posts in <?php echo $category->name ?>"><?php echo $category->name; ?></a>
							
							<?php
								$args = array('parent' => $category->term_id);
								$sub_categories = get_categories( $args );
								if (count($sub_categories) > 0):
							?>
								<ul class="sub-categories">
									
							<?php foreach($sub_categories as $sub_category) { 
								$sub_category_link = get_category_link( $sub_category->term_id );		
							?>
								<li>
									<a class="category-title<?php if ( has_category($sub_category,$post->ID) && is_single() ):?> active<?php endif; ?>" href="<?php echo esc_url($sub_category_link); ?>" title="View all posts in <?php echo $sub_category->name ?>"><?php echo $sub_category->name; ?></a>
								</li>
							<?php } ?>
									
								</ul>
							
							<?php endif; ?>
							
						</li>
					<?php } wp_reset_postdata(); ?>
					<div class="clear"></div>
				</ul>

			</nav>
			
		</header>
