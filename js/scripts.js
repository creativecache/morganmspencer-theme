jQuery(document).ready(function($) {
	
	function mobileFunctions() {
		if ($('.mobile-menu').is(':visible')) {
			
		} else {
			$('#main-nav').css('display','');
			$('#category-nav').css('display','');
			$('#main-nav').css('height','');
			$('#category-nav').css('height','');
		}
	}
	
	function scrollToTop($value) {
		$('html, body').animate({
			scrollTop: $($value).offset().top - 30
		 }, 750);
	}
	
	$('a').each(function() {
		if ($(this).attr('target') == '_blank') {
			$(this).attr('rel', 'nofollow noreferrer');
		}	
	})
	
	$('#mms-social .more-sharing').click(function(e) {
		e.preventDefault();
		//$('#mms-social .more-sharing').children('i').toggleClass('fa-plus fa-minus');
		$('#mms-social .more-sharing i').each(function() {
			$(this).toggleClass('fa-plus fa-minus');
		});
		$('#mms-social .extra-sharing').toggleClass('visible');
	});
	
	$('#main-nav li a[target="_blank"]').append('<i class="fas fa-external-link-alt"></i>');
	
	$('#main-nav li.menu-item-has-children > a').append('<i class="fas fa-chevron-down"></i>');

	$('#category-nav .sub-categories').prev().append('<i class="fas fa-chevron-down"></i>');
		
	$('header .search-toggle').click(function(e) {
		e.preventDefault();
		$('header .search').fadeIn(500);
		$('header .search-input').focus();
	});
	
	$('.mobile-menu').click(function(e) {
		e.preventDefault();
		$('#main-nav').stop().slideToggle(400);
		$('#category-nav').stop().slideToggle(400);
	});
	
	$('a[href^="#"]').each(function() {
		$(this).click(function(e) {
			e.preventDefault();
			var href = $(this).attr('href');
			scrollToTop(href);
		});
	});
	
	// Mailchimp submit email validation
	
	$('form[name="mc-embedded-subscribe-form"]').each(function() {
		$(this).find("#mc-embedded-subscribe").prop('disabled', true);
	});
	
	$('form[name="mc-embedded-subscribe-form"] input[type="email"]').on('input', function() {
		
			var email = $(this).val();
			function validateEmail(email) {
				var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				return regex.test(email)
			}

			if (validateEmail(email)) {
				$(this).parents('form').find("#mc-embedded-subscribe").prop('disabled', false);
			} else {
				$(this).parents('form').find("#mc-embedded-subscribe").prop('disabled', true);
			}
	
			return false;
		
	});

	$('main, #category-nav').click(function() {
		if ($('header .search').is(':visible')) {
			$('header .search').fadeOut(500);
		}
	});
	
	
	// Functions to call on page load
	mobileFunctions();
	//footerBottom();

	//Functions to call when window is resized
	jQuery(window).resize(function($) {
		mobileFunctions();
		//footerBottom();
	});

	//Functions to call when window is scrolled
	jQuery(window).scroll(function($) {
		
	});
	
});
